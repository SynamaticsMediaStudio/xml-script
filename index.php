<?php 
header('Content-Type: application/json; charset=utf-8');

include "xmlarray.php";

/**
 * @return array
 */
function fetchData() {
    $rss = simplexml_load_string(file_get_contents("http://snopeo.com/feed.xml"));

    $items = array();
    foreach ($rss->channel->item as $item) {
        $item_data = array(
            "title" => (string)$item->title,
            "description" => (string)$item->description,
            "link" => (string)$item->link,
            "guid" => (string)$item->guid,
        );
        $items[] = $item_data;
    }

    return $items;
}

$xmlarray = fetchData();
echo json_encode($xmlarray);


