<?php

/**
 * This class is responsible for parsing XML data and returning it as a JSON string.
 */
class XMLparse {
    /**
     * This is the URL of the XML file to be parsed.
     * @var string
     */
    private static string $url;

    /**
     * This array will hold the parsed XML data.
     * @var array
     */
    private static array $fileattr = [];

    /**
     * This method takes a URL as input and returns the parsed XML data as a JSON string.
     * @param string $data The URL of the XML file to be parsed.
     * @return string The parsed XML data as a JSON string.
     */
    public static function require(string $data): string {
        self::$url = $data;

        libxml_use_internal_errors(true);

        $xml = simplexml_load_file(self::$url, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_PARSEHUGE);

        libxml_clear_errors();

        if ($xml === false) {
            die("Error: Cannot create object");
        }

        self::$fileattr = json_decode(json_encode($xml), true);

        return json_encode(self::$fileattr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}

